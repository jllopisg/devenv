#!/usr/bin/env bash
# vi: ts=4 sw=4 et filetype=sh

REPOFILE=https://bitbucket.org/jllopis/devenv/get/master.tar.bz2
GETTER=`type -p curl`
GETTER_OPTS="-# -LkO"
if [ -z "$GETTER" ]; then
    GETTER=`type -p wget`
    GETTER_OPTS=
fi

get_os() {
    case `uname` in
        'Darwin')
            OSNAME=darwin
        ;;
        'Linux')
            OSNAME=linux
        ;;
    esac
}

install_ansible(){
    PIP=`type -p pip`
    case "$OSNAME" in
        'darwin')
            if [ -z "$PIP" ]; then
                sudo easy_install pip
            fi
            sudo pip install ansible
        ;;
        'linux')
            if [ -z "$PIP" ]; then
                sudo apt-get install -y python-pip
            fi
            sudo pip install ansible
        ;;
    esac
}

check_or_install() {
    case "$1" in
        'python')
             PYTHON=`type -p python`
             if [ -z "$PYTHON" ]; then
                 $ECHO "[x] Python not found. Must be installed to proceed. Exiting!"
                 exit 1
             fi
         ;;
         'ansible')
             ANSIBLE=`type -p ansible-playbook`
             if [ -z "$ANSIBLE" ]; then
                 $ECHO "[x] Ansible not found. Must be installed to proceed. Installing..."
                 install_ansible
                 ANSIBLE=`type -p ansible-playbook`
                 if [ -z "$ANSIBLE" ]; then $ECHO "[x] Can not install Ansible. Exiting" ; exit 1; fi
             fi
         ;;
    esac
}

# Check OS we're in
get_os
ECHO="echo"
#if [ "$OSNAME" == "darwin" ]; then
#    ECHO="gecho"
#fi
$ECHO "[O] Running under ${OSNAME}"

# Check if python is installed and install it if it isn't
check_or_install python
$ECHO "[O] Found Python at ${PYTHON}"

# Check if ansible is installed and install it if it isn't
check_or_install ansible
$ECHO "[O] Found ansible-playbook at ${ANSIBLE}"

# Download repository tar.gz file
$ECHO "Downloading file: $GETTER $OPTS $REPOFILE"
$GETTER $GETTER_OPTS $REPOFILE

# get the main directory in the archive
REPO_DIR=`basename $(tar -jtf master.tar.bz2 | head -n1)`

# Uncompress at default location
bunzip2 -c master.tar.bz2 |tar xvf - &> /dev/null
cd $REPO_DIR

# Ask git name and email
$ECHO -n "Insert your Git user name: "
read GITNAME

$ECHO -n "Insert your Git email: "
read GITEMAIL

$ECHO "[O] Your gitconfig will be configured with:"
$ECHO -e "\tgitname: $GITNAME"
$ECHO -e "\tgitemail: $GITEMAIL"

# Substitute git name and email at roles/base/defaults/main.yml
sed -i.bak s/"My name"/"$GITNAME"/g roles/base/defaults/main.yml
sed -i.bak s/"my@own.mail"/"$GITEMAIL"/g roles/base/defaults/main.yml

# run playbook
$ECHO "[O] setup complete. Now going to run playbook"
$ECHO ""
$ECHO "To install some packages, the sudo password is needed (your password)"

ansible-playbook setup_devenv.yml -K

# show a nice welcome message :)
$ECHO ""
$ECHO ""
$ECHO "=============================="
$ECHO "[O] INSTALLATION COMPLETED!!"

exit 0
