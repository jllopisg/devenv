# vi: ts=4 sw=4 noet filetype=sh
# Default settings for bash
echo -e "${information}Loading default bash settings${text_reset}"

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Append to the Bash history file, rather than overwriting it
shopt -s histappend

# Autocorrect typos in path names when using `cd`
shopt -s cdspell

# `autocd`, e.g. `**/qux` will enter `./foo/bar/baz/qux`
shopt -s autocd

# Recursive globbing, e.g. `echo **/*.txt`
shopt -s globstar

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2 | tr ' ' '\n')" scp sftp ssh

# Larger bash history (allow 32³ entries; default is 500)
export HISTSIZE=32768
export HISTFILESIZE=$HISTSIZE
export HISTCONTROL=ignoredups
# Make some commands not show up in history
export HISTIGNORE="ls:cd:cd -:pwd:exit:date:* --help"

# Prefer ES Catalan and use UTF-8
export LANG="ca_ES.UTF-8"
export LC_ALL="ca_ES.UTF-8"

# Highlight section titles in manual pages
export LESS_TERMCAP_md="${bold_orange}"

# Don’t clear the screen after quitting a manual page
export MANPAGER="less -X"

if [[ $COLORTERM = gnome-* && $TERM = xterm ]] && infocmp gnome-256color >/dev/null 2>&1; then
	export TERM=gnome-256color
elif infocmp xterm-256color >/dev/null 2>&1; then
	export TERM=xterm-256color
fi

export DOCKER_HOST=tcp://127.0.0.1:2375
alias drm='docker ps -a | grep "Exit" | cut -d " " -f 1 | xargs docker rm'
