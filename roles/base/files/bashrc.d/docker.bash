case `uname` in
	'Darwin')
		export DOCKER_HOST=${DOCKER_HOST}
		export DOCKER_CERT_PATH=${DOCKER_CERT_PATH}
		export DOCKER_TLS_VERIFY=1
	;;
esac
