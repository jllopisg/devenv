# vi: ts=4 sw=4 noet filetype=sh
echo -e "${information}Setting aliases${text_reset}"

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

