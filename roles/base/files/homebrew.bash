# vi: ts=4 sw=4 noet filetype=sh
echo -e "${information}(Homebrew) Loading bash completion${text_reset}"
# Brew bash completion
if [ -f `brew --repository`/Library/Contributions/brew_bash_completion.sh ]; then
	. `brew --repository`/Library/Contributions/brew_bash_completion.sh
fi

echo -e "${information}(Homebrew) Injecting /usr/local/sbin to PATH${text_reset}"
export PATH="/usr/local/sbin:$PATH"


