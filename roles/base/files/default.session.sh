SESSION_PATH=$HOME/devel
SESSION_ID=default

# Main window
mux_nw "main"

# Secondary window
mux_nw "edit" "vim"

# Create right pane (#1) on edit window
mux_split_h "edit" "30"

# Set focus on main window
mux_wfocus "main"

# Welcome msg on main window
mux_sk "main" "echo 'Ladies and Gentlemen, Welcome... to violence'"

# Set focus on edit pane (edit window)
mux_pfocus "edit.0"
