if [[ ! -d "$HOME/devel/go" ]]; then
    for d in {ext,own}/bin {ext,own}/pkg {ext,own}/src; do
        mkdir -p $HOME/devel/go/$d
    done
fi

export GOROOT="/usr/local/go"
export GOPATH="$HOME/devel/go/ext:$HOME/devel/go/own"
pathmunge /usr/local/go/bin
pathmunge $HOME/devel/go/ext/bin
pathmunge $HOME/devel/go/own/bin

